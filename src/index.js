import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {I18nextProvider} from 'react-i18next';
import i18next from 'i18next';
import registerServiceWorker from './registerServiceWorker';
import common_ua from "./translations/ua/common.json";
import common_en from "./translations/en/common.json";

i18next.init({
  interpolation: { escapeValue: false },  // React already does escaping
  lng: 'en',                              // language to use
  resources: {
    en: {
      common: common_en               // 'common' is our custom namespace
    },
    ua: {
      common: common_ua
    },
  },
});

ReactDOM.render(
  <I18nextProvider i18n={i18next} initialLanguage={'en'}>
    <App />
  </I18nextProvider>,
  document.getElementById('root'));
registerServiceWorker();
