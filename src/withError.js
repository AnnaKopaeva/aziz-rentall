import React from 'react';

import ModalContext from "./context/ModalContext";

const withError = (Component) => (
  class withError extends React.Component {
    toggleErrorModal = (error) => {
      this.setState({
        openErrorModal: !this.state.openErrorModal,
        errorDescription: error
      })
    };

    state = {
      openErrorModal: false,
      errorDescription: "",
      toggleErrorModal: this.toggleErrorModal,
    };

    render() {
      return (
        <ModalContext.Provider value={this.state}>
          <Component />
        </ModalContext.Provider>
      );
    }
  }
)

export default withError;