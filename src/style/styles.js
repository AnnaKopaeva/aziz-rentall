import styled from 'styled-components';

export const customStyles = {
  content : {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)'
  }
};

export const Wrapper = styled.div`
   width: 500px;
   margin: auto;
   border: 1px solid;
   padding: 10px;
   margin-bottom: 10px;
`;

export const Input = styled.input`
  width: 300px;
  padding: 10px;
  border: 1px solid #e5e5e5;
  outline: none;
  box-sizing: border-box;
  display: ${props => props.file ? "none" : "block"};
`;

export const Label = styled.label`
  width: 100px;
  padding: 10px;
  color: white;
  border-radius: 5px;
  background-color: #0388ca;
  box-sizing: border-box;
`;

export const Textarea = styled.textarea`
  width: 300px;
  padding: 10px;
  border: 1px solid #e5e5e5;
  outline: none;
  box-sizing: border-box;
`;

export const Select = styled.select`
  width: 300px;
  padding: 10px;
  border: 1px solid #e5e5e5;
  outline: none;
  box-sizing: border-box;
`;

export const Button = styled.button`
  background-color: #0388ca;
  width: ${props => props.auth ? "100px" : "300px"};
  padding: 10px;
  margin: ${props => props.auth ? "10px" : "0px"};
  border: none;
  outline: none;
  color: white;
  cursor: pointer;
  font-weight: bold;
  border-radius: 5px;
  transition: background-color, color 0.5s ease-out;
  &:disabled, &:hover {
    background-color: #e5e5e5;
    color: #696969;
  }
`;

export const Title = styled.h3`
  text-align: center;
  color: #696969;
`;

export const Link = styled.a`
  font-weight: bold;
  color: #0388ca;
  padding: 0 5px;
`;

export const Image = styled.img`
  width: 50px;
  height: 40px;
  margin-left: 5px;
`;

export const FormRow = styled.div`
  width: ${props => props.auth ? "300px" : "500px"};
  box-sizing: border-box;
  padding: 5px 0px;
  display: flex;
  justify-content: ${props => props.center ? "center" : "space-between"};
  
`;
