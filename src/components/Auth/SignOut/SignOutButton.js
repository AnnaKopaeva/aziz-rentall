import React from "react";
import {auth} from "../../../firebase";
import { Button } from "../../../style/styles"

const SignOutButton = () => (
  <Button auth onClick={auth.doSignOut}>Sign out</Button>
);

export default SignOutButton;