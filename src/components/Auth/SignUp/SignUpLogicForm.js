import SignUpForm from "./SignUpForm";
import {withFormik} from "formik/dist/index";

import equalTo from "../../../utility/equalToPassword";
import {string, number, object, ref, addMethod} from "yup";
import { auth, db } from '../../../firebase';
import "../auth.css";

addMethod(string, "equalTo", equalTo);

const SignUpLogicForm = withFormik({
  mapPropsToValues: () => ({
    username: "",
    email: "",
    password: "",
    confirmPassword: "",
    phone: "",
  }),
  validationSchema: object().shape({
    username: string().required("Username is required!"),
    email: string().email().required("Email is required!"),
    password: string().min(6, "Password is too short"),
    confirmPassword: string().equalTo(ref("password"), "Passwords must match"),
    phone: number().required('phone is required'),
  }),
  handleSubmit: async (values, { props, setSubmitting }) => {
    auth.doCreateUserWithEmailAndPassword(values.email, values.password)
      .then(authUser => {
        db.collection("users").doc(authUser.user.uid).set({
          email: values.email,
          phoneNumber: values.phone,
          username: values.username,
          isAdmin: false
        })
          .catch(error => {
            props.toggleErrorModal(error.message);
          });
      })
      .catch(error => {
        props.toggleErrorModal(error.message);
      });
    setSubmitting(false);
  },
})(SignUpForm);

export default SignUpLogicForm;