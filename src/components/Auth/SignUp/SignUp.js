import React from "react";
import SignUpLogicForm from "./SignUpLogicForm";
import PropTypes from "prop-types";
import ErrorModal from "../../Modal/ErrorModal";
import "./signUp.css";

const SignUp = (props) => (
  <div className="signUp">
    <div className="headerSignUp">
      <h3 className="title">Sign Up</h3>
      <span
        className="link"
        onClick={() => props.changeScreenAuth(true, false)}>
        Back
      </span>
    </div>
    <SignUpLogicForm
      toggleErrorModal={props.toggleErrorModal}
    />
    <ErrorModal />
  </div>
);

SignUp.propTypes = {
  toggleModal: PropTypes.func,
};

export default SignUp
