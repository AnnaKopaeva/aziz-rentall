import React, { Component } from "react";
import PropTypes from "prop-types"

class SignUpForm extends Component {
  render() {
    const {
      values,
      touched,
      errors,
      handleSubmit,
      handleChange,
      handleBlur,
      dirty
    } = this.props;

    return (
      <form className="form" onSubmit={handleSubmit}>
        <input
          name="username"
          placeholder="Username"
          type="text"
          value={values.username}
          onChange={handleChange}
          onBlur={handleBlur}
          className={
            errors.username && touched.username
              ? "form__field error"
              : "form__field"
          }
        />
        {errors.username &&
          touched.username &&
            <span style={{ color: "red"}}>{errors.username}</span>
        }
        <input
          name="email"
          placeholder="Email"
          type="text"
          value={values.email}
          onChange={handleChange}
          onBlur={handleBlur}
          className={
            errors.email && touched.email
              ? "form__field error"
              : "form__field"
          }
        />
        {errors.email &&
          touched.email &&
            <span style={{ color: "red"}}>{errors.email}</span>
        }
        <input
          name="password"
          placeholder="Password"
          type="password"
          value={values.password}
          onChange={handleChange}
          onBlur={handleBlur}
          className={
            errors.password && touched.password
              ? "form__field error"
              : "form__field"
          }
        />
        {errors.password &&
          touched.password &&
            <span style={{ color: "red"}}>{errors.password}</span>
        }
        <input
          name="confirmPassword"
          placeholder="Confirm password"
          type="password"
          value={values.confirmPassword}
          onChange={handleChange}
          onBlur={handleBlur}
          className={
            errors.confirmPassword && touched.confirmPassword
              ? "form__field error"
              : "form__field"
          }
        />
        {errors.confirmPassword &&
          touched.confirmPassword &&
              <span style={{ color: "red" }}>{errors.confirmPassword}</span>
        }
        <input
          name="phone"
          placeholder="Phone number"
          type="text"
          value={values.phone}
          onChange={handleChange}
          onBlur={handleBlur}
          className={
            errors.phone && touched.phone
              ? "form__field error"
              : "form__field"
          }
        />
        {errors.phone &&
          touched.phone &&
              <span style={{ color: "red" }}>{errors.phone}</span>
        }
        <button
          className="btn submit"
          type="submit"
          disabled={!dirty}
        >
          Sign up
        </button>
      </form>
    );
  }
}

SignUpForm.propTypes = {
  values: PropTypes.object,
  touched: PropTypes.object,
  errors: PropTypes.object,
  handleSubmit: PropTypes.func,
  handleChange: PropTypes.func,
  handleBlur: PropTypes.func,
  dirty: PropTypes.bool,
};

export default SignUpForm;
