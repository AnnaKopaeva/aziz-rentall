import React, { Fragment, Component } from 'react';
import Login from "../../containers/Auth/Login/LoginLogic";
import SignUp from "../../containers/Auth/SignUp/SignUpLogic";

class Auth extends Component {
  state = {
    login: true,
    signup: false
  };

  changeScreenAuth = (login, signup) => {
    this.setState({login, signup})
  };

  render() {
    const { login, signup } = this.state;
    return (
      <Fragment>
        {login ?
          <Login
            changeScreenAuth={this.changeScreenAuth}
          />
        : null}
        {signup ?
          <SignUp
            changeScreenAuth={this.changeScreenAuth}
          />
        : null}
      </Fragment>
    )
  }
};

export default Auth;