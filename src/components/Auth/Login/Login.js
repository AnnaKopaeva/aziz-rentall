import React from "react";
import LoginLogicForm from "./LoginLogicForm";
import PropTypes from "prop-types";
import ErrorModal from "../../Modal/ErrorModal";
import "./login.css";

const Login = (props) => (
  <div className="login">
    <h3 className="title">Login</h3>
    <LoginLogicForm
      toggleErrorModal={props.toggleErrorModal}
    />
    <p className="textLink">
      New user?
      <span
        className="link active"
        onClick={() =>props.changeScreenAuth(false, true)}
      >
        Sign Up
      </span>
    </p>
    <ErrorModal />
  </div>
);

Login.propTypes = {
  toggleModal: PropTypes.func,
};

export default Login
