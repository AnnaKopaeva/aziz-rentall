import { withFormik } from "formik";
import {string, addMethod} from "yup";
import LoginForm from "./LoginForm";
import equalTo from "../../../utility/equalToPassword";
import { auth } from '../../../firebase';
addMethod(string, "equalTo", equalTo);

const LoginLogicForm = withFormik({
  mapPropsToValues: () => ({
    email: "",
    password: "",
  }),
  handleSubmit: async (values, { setSubmitting, props }) => {
    auth.doSignInWithEmailAndPassword(values.email, values.password)
      .catch(error => {
        props.toggleErrorModal(error.message);
      });
    setSubmitting(false);
  },
})(LoginForm);

export default LoginLogicForm;