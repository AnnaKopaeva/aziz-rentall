import React, { Component } from "react";
import PropTypes from "prop-types"

class LoginForm extends Component {
  render() {
    const {
      values,
      touched,
      errors,
      handleSubmit,
      handleChange,
      handleBlur,
    } = this.props;

    return (
      <form className="form" onSubmit={handleSubmit}>
        <input
          name="email"
          placeholder="Email Address"
          type="text"
          value={values.email}
          onChange={handleChange}
          onBlur={handleBlur}
          className={
            errors.email && touched.email
              ? "form__field error"
              : "form__field"
          }
        />
        {errors.email &&
          touched.email &&
            <span style={{ color: "red"}}>{errors.email}</span>
        }
        <input
          name="password"
          placeholder="Password"
          type="password"
          value={values.password}
          onChange={handleChange}
          onBlur={handleBlur}
          className={
            errors.password && touched.password
              ? "form__field error"
              : "form__field"
          }
        />
        {errors.password &&
          touched.password &&
            <span style={{ color: "red"}}>{errors.password}</span>
        }
        <p className="link forgot">
          Forgot password?
        </p>
        <button
          className="btn submit"
          type="submit"
          disabled={!values.email || !values.password}>
          Login
        </button>
      </form>
    );
  }
}

LoginForm.propTypes = {
  values: PropTypes.object,
  touched: PropTypes.object,
  errors: PropTypes.object,
  handleSubmit: PropTypes.func,
  handleChange: PropTypes.func,
  handleBlur: PropTypes.func,
};

export default LoginForm;
