import React from "react";
import { Title, customStyles, Link } from "../../style/styles";
import ModalContext from "../../context/ModalContext";
import Modal from 'react-modal';

const ErrorModal = () => (
  <ModalContext.Consumer>
    { value => (
      <Modal
        isOpen={value.openErrorModal}
        contentLabel="Error"
        appElement={document.getElementById('root')}
        style={customStyles}>
        <Link onClick={() => value.toggleErrorModal()}>Close</Link>
        <Title>{value.errorDescription}</Title>
      </Modal>
    )}
  </ModalContext.Consumer>
);

export default ErrorModal;