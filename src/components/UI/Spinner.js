import React from "react";
import { Wrapper, Image } from "../../style/styles";
import SpinnerImage from "../../images/spinner.gif";

const Spinner = () => (
  <Wrapper>
    <Image src={SpinnerImage} alt="spinner"/>
  </Wrapper>
);

export default Spinner;