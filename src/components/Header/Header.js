import React, { Component } from 'react';
import PropTypes from "prop-types";
import {translate} from "react-i18next";
import "./Header.css";
import SignOutButton from '../Auth/SignOut/SignOutButton';

class Header extends Component {
  state = {
    language: ["ua", "en"],
    activeBtn: "ua",
  };

  changeLanguage = (lng) => {
    const { i18n } = this.props;
    this.setState({activeBtn: lng});
    i18n.changeLanguage(lng);
  };


  render() {
    const { activeBtn, language } = this.state;
    const { authUser } = this.props;
    const listLang = language.map(lng =>
      <div
        key={lng}
        className={lng === activeBtn ? "lngBtn active" : "lngBtn"}
        onClick={() => this.changeLanguage(lng)}>
        {lng}
      </div>
    );
    return (
      <header className="header">
        { authUser ?
          <SignOutButton />
        : null}
        {listLang}
      </header>
    )
  }
}

export default translate("common")(Header);
