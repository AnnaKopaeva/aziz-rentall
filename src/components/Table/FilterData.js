import React from 'react';
import { Select, FormRow, Input, Wrapper } from "../../style/styles.js";

const FilterData = (props) => (
  <Wrapper>
    <FormRow>
      <span>Title</span>
      <Input
        type="text"
        value={props.title}
        onChange={(e) => props.changeTitle(e)}
      />
    </FormRow>
    <FormRow>
      <span>Status</span>
      <Select onChange={e => props.changeStatus(e)}>
        <option
          value="all">
          All
        </option>
        <option
          value="active">
          Active
        </option>
        <option
          value="suspended">
          Suspended
        </option>
      </Select>
    </FormRow>
    <FormRow>
      <button
        onClick={props.applyFilters}>
        Apply
      </button>
    </FormRow>
  </Wrapper>
);

export default FilterData;