import React, { Component } from 'react';
import PropTypes from "prop-types";
import {db} from "../../firebase";
import { translate } from "react-i18next";
import PaginationButton from "./PaginationButton";
import FilterContext from "../../context/FilterContext";

class Pagination extends Component {
  state = {
    id: null,
  };

  componentDidMount(){
    const { authUser } = this.props;
    const changeId = (id) => {
      this.setState({id})
    };

    db.collection("items").doc(authUser.uid).collection("tasks")
      .orderBy('id', "desc")
      .limit(1).get().then(snapshot => {
      snapshot.forEach(function(doc) {
        changeId(doc.id);
      });
    });
  }

  render() {
    const { id } = this.state;
    const { numberPerPage, authUser, updateData, changeVisible, lastVisible, toggleErrorModal, firstVisible } = this.props;

    return (
      <FilterContext.Consumer>
        {value =>
          <PaginationButton
            title={value.title}
            status={value.status}
            id={id}
            numberPerPage={numberPerPage}
            authUser={authUser}
            updateData={updateData}
            changeVisible={changeVisible}
            lastVisible={lastVisible}
            firstVisible={firstVisible}
            toggleErrorModal={toggleErrorModal}
          />
        }
      </FilterContext.Consumer>
    )
  }
}

Pagination.propTypes = {
  numberPerPage: PropTypes.number,
  authUser: PropTypes.object,
  updateData: PropTypes.func,
};

export default translate("common")(Pagination);