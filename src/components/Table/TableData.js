import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from "prop-types";
import { translate } from "react-i18next";
import updateRealData from "../../utility/updateRealData";
import deleteItem from "../../utility/deleteItem";
import suspendedItem from "../../utility/suspendedItem";

class Table extends Component {
  updateRealData = () => {
    const {
      numberPerPage,
      authUser,
      updateData,
      firstVisible,
      changeVisible,
      toggleErrorModal,
      listOfItems
    } = this.props;

    updateRealData(
      authUser,
      listOfItems,
      firstVisible,
      numberPerPage,
      updateData,
      changeVisible,
      toggleErrorModal
    )
  };

  deleteItem = (autoId, id) => {
    const {
      authUser,
      listOfItems,
      updateData,
      toggleErrorModal
    } = this.props;
    const { updateRealData } = this;

    deleteItem(
      authUser,
      listOfItems,
      autoId,
      id,
      updateData,
      updateRealData,
      toggleErrorModal
    );
  };

  suspendedItem = (autoId, id, suspended) => {
    const {
      authUser,
      listOfItems,
      updateData,
      toggleErrorModal
    } = this.props;

    suspendedItem(
      authUser,
      listOfItems,
      autoId,
      id,
      suspended,
      updateData,
      toggleErrorModal
    );
  };

  listData(data) {
    const { t } = this.props;
    if (data) {
      return data.map(data =>
        <tr key={data.id}>
          <td>{data.id}</td>
          <td>{data.title}</td>
          <td>{data.suspended ? "suspended" : "active" }</td>
          <td>
            <button
              onClick={() => this.deleteItem(data.autoId, data.id)}>
              {t('table.delete')}
            </button>
            <button
              onClick={() => this.suspendedItem(data.autoId, data.id, data.suspended)}>
              {t('table.suspended')}
            </button>
            <Link to={`view/${data.autoId}`}>
              View
            </Link>
          </td>
        </tr>
      )
    }
  }

  render() {
    const { listOfItems, t } = this.props;

    return (
      <table>
        <tbody>
        <tr>
          <th>{t('table.item')}</th>
          <th>{t('table.title')}</th>
          <th>{t('table.status')}</th>
          <th>{t('table.actions')}</th>
        </tr>
        {this.listData(listOfItems)}
        </tbody>
      </table>
    )
  }
}

Table.propTypes = {
  numberPerPage: PropTypes.number,
  authUser: PropTypes.object,
  updateData: PropTypes.func,
};

export default translate("common")(Table);