import React, { Component } from 'react';
import PropTypes from "prop-types";
import { translate } from "react-i18next";
import uploadNextData from "../../utility/uploadNextData";
import uploadPrevData from "../../utility/uploadPrevData";

class Table extends Component {
  state = {
    disableNext: false,
    disablePrev: true,
  };

  showNextList = async () => {
    const { numberPerPage, authUser, updateData, changeVisible, lastVisible, toggleErrorModal } = this.props;

    const changeDisable = (next, prev) => {
      this.setState({disableNext: next, disablePrev: prev})
    };

    uploadNextData(authUser,  lastVisible, numberPerPage)
      .then(list => {
        updateData(list.arrItems);
        changeVisible(list.lastVisible, list.firstVisible);
        changeDisable(false, false);
        if (list.arrItems.length < numberPerPage) {
          changeDisable(true, false);
        }
      })
      .catch(() => toggleErrorModal("Error loading documents"))
  };

  showPrevList = () => {
    const { numberPerPage, updateData, authUser, changeVisible, firstVisible, toggleErrorModal, id } = this.props;

    const changeDisable = (next, prev) => {
      this.setState({disableNext: next, disablePrev: prev})
    };

    uploadPrevData(authUser, firstVisible, numberPerPage, id, changeDisable)
      .then(list => {
        updateData(list.arrItems.reverse());
        changeVisible(list.lastVisible, list.firstVisible);
        changeDisable(false, false);
        if (list.firstVisible && list.firstVisible.id === id) {
          changeDisable(false, true);
        }
        if (list.arrItems.length !== 0) {
          changeDisable(false, true);
        }
      })
      .catch((error) => toggleErrorModal("Error loading documents", error))
  };

  render() {
    const { disableNext, disablePrev } = this.state;
    const { t } = this.props;

    return (
      <div>
        <button
          onClick={this.showPrevList}
          disabled={disablePrev}>
          {t("pagination.prev")}
        </button>
        <button
          onClick={this.showNextList}
          disabled={disableNext}>
          {t("pagination.next")}
        </button>
      </div>
    )
  }
}

Table.propTypes = {
  numberPerPage: PropTypes.number,
  authUser: PropTypes.object,
  updateData: PropTypes.func,
};

export default translate("common")(Table);