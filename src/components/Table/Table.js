import React, { Fragment } from 'react';
import { Wrapper } from "../../style/styles";
import PropTypes from "prop-types";
import Pagination from "./Pagination";
import TableData from "./TableData";

const Table = props => (
  <Fragment>
    { props.listOfItems.length !== 0 ? (
      <Wrapper>
        <TableData
          listOfItems={props.listOfItems}
          authUser={props.authUser}
          firstVisible={props.firstVisible}
          numberPerPage={props.numberPerPage}
          updateData={props.updateData}
          changeVisible={props.changeVisible}
          toggleErrorModal={props.toggleErrorModal}
        />
        { !props.filter ? (
          <Pagination
            authUser={props.authUser}
            numberPerPage={props.numberPerPage}
            updateData={props.updateData}
            changeVisible={props.changeVisible}
            lastVisible={props.lastVisible}
            firstVisible={props.firstVisible}
            toggleErrorModal={props.toggleErrorModal}
          />
        ): null }
      </Wrapper>
    ) : null}
  </Fragment>
);

Table.propTypes = {
  numberPerPage: PropTypes.number,
  authUser: PropTypes.object,
  updateData: PropTypes.func,
};

export default Table;