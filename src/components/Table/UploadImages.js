import React, { Component } from 'react';
import { Image, FormRow, Input, Label } from "../../style/styles.js";
import firebase from 'firebase';
import PropTypes from "prop-types";

class UploadImages extends Component {
  state = {
    file: ""
  };

  uploadImages = (event) => {
    event.preventDefault();

    let reader = new FileReader();
    let file = event.target.files[0];
    let storageRef = firebase.storage().ref();
    const { changeListImages, uploadedImageHandler, toggleErrorModal, authUser, id, updateListImages } = this.props;

    const changeFileValue = (value) => {
      this.setState({file: value});
    };
    changeFileValue(event.target.files[0] && event.target.files[0].name);

    reader.onloadend = () => {
      const {result} = reader;
      updateListImages(result)
    };

    uploadedImageHandler(false);

    if (file) {
      const starsRef = storageRef.child(`images/${authUser.uid}/${id}/${file.name}`);
      storageRef.child(`images/${authUser.uid}/${id}/` + file.name).put(file)
        .then(() => {
          starsRef.getDownloadURL().then(url => {
            changeListImages(url);
            uploadedImageHandler(true);
            changeFileValue("");
          })
        })
        .catch(error =>  {
          toggleErrorModal(error.message);
        });
      reader.readAsDataURL(file);
    }

  };

  render() {
    const { infoImagesArray } = this.props;

    const { infoImages } = this.props;
    const { t } = this.props;
    let listImages;
    if (infoImages.length !== 0) {
      listImages = infoImagesArray.map(image =>
        <Image
          key={image.key}
          src={image.url}
          alt="user load" />
      );
    }

    return (
      <FormRow>
        <span>{t("addItem.uploadImage")}</span>
        <Label>
          <Input
            type="file"
            file
            value={this.state.value}
            onChange={e => this.uploadImages(e)}
            disabled={infoImages.length > 4}
          />
          Choose file
        </Label>
        {listImages}
      </FormRow>
    )
  }
}

UploadImages.propTypes = {
  changeListImages: PropTypes.func,
};

export default UploadImages;