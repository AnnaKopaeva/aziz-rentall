import React, {Fragment} from 'react';
import { Link } from 'react-router-dom';
import Table from "../../containers/Table/TableLogic";
import Spinner from "../UI/Spinner";
import FiltersLogic from "../../containers/Filters/FiltersLogic";

const Content = (props) => (
  <div>
    <Link to="/add">Add new</Link>
    <FiltersLogic
      authUser={props.authUser}
    />
    {props.loading ?
      <Spinner/> :
      <Table
        authUser={props.authUser}
      />
    }
  </div>
);

export default Content;