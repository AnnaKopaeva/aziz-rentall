import React, { Fragment } from 'react';
import ErrorModal from "../Modal/ErrorModal";
import Content from "../../containers/Content/ContentLogic";
import Header from "../Header/Header";
import Auth from "../Auth/Auth"
import "../../style/styles.css";

const Main = props => (
  <Fragment>
    <Header authUser={props.authUser} />
    <ErrorModal />
    <div className="content">
      {props.authUser ?
        <Content
          authUser={props.authUser}
          toggleErrorModal={props.toggleErrorModal}
        />
      : <Auth />}
    </div>
  </Fragment>
);

export default Main;