import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import UploadImages from "../Table/UploadImages";
import { db } from "../../firebase/index";
import { Wrapper, FormRow, Button, Input, Textarea, Select } from "../../style/styles";
import PropTypes from "prop-types";
import { translate } from "react-i18next";

class AddItem extends Component {
  state = {
    category: "category1",
    fullDescription: "",
    shortDescription: "",
    title: "",
    infoImages: [],
    infoImagesArray: [],
    id: 0,
    suspended: false,
    uploadedImage: true
  };

  componentDidMount(){
    this.changeLastItemsId();
  }

  changeLastItemsId = () => {
    const { authUser } = this.props;

    const changeId = (id) => {
      this.setState({id: id + 1})
    };

    db.collection("items").doc(authUser.uid).collection("tasks")
      .orderBy('id', 'desc')
      .limit(1).get().then(snapshot => {
      snapshot.forEach(doc => {
        changeId(doc.data().id);
      });
    });
  };

  uploadedImageHandler = (uploadedImage) => {
    this.setState({uploadedImage})
  };

  changeListImages = (url) => {
    const { infoImages } = this.state;
    this.setState({
      infoImages: [
        ...infoImages,
        {
          url,
          key: new Date().getTime()
        },
      ]
    })
  };

  updateListImages = (result) => {
    this.setState({
      infoImagesArray:
        [...this.state.infoImagesArray,
          {
            url: result,
            key: new Date().getTime()
          }
        ]
    });
  };

  saveData = (e) => {
    e.preventDefault();
    const { category, fullDescription, shortDescription, title, infoImages, id, suspended } = this.state;
    const { toggleErrorModal, authUser, history } = this.props;
    const { changeLastItemsId } = this;
    const clearForm = () => {
      this.setState({title: "", fullDescription: "", shortDescription: "", infoImages: [], infoImagesArray: []})
    };

    db.collection("items").doc(authUser.uid).collection("tasks").add({
      category,
      fullDescription,
      id,
      shortDescription,
      title,
      suspended,
      infoImages
    })
      .then(() => {
        changeLastItemsId();
        clearForm();
        history.push('/');
      })
      .catch(error => {
        toggleErrorModal(error.message);
      });
  };

  valueTitleHandler = (event) => {
    this.setState({title: event.target.value})
  };

  valueShortDescHandler = (event) => {
    this.setState({shortDescription: event.target.value})
  };

  valueFullDescHandler = (event) => {
    this.setState({fullDescription: event.target.value})
  };

  handleChange = e => {
    this.setState({ category: e.target.value });
  };

  render(){
    const { fullDescription, shortDescription, title, uploadedImage, infoImages, id, infoImagesArray } = this.state;
    const { t, toggleErrorModal, authUser } = this.props;
    const disable =
      fullDescription === "" ||
      shortDescription === "" ||
      title === "" ||
      !uploadedImage;

    return (
      <Wrapper>
        <Link to="/">Back</Link>
        <h3>{t("addItem.title")}</h3>
        <form onSubmit={e => this.saveData(e)}>
          <FormRow>
            <span>{t("addItem.itemTitle")}</span>
            <Input
              type="text"
              value={title}
              onChange={(e) => this.valueTitleHandler(e)}
            />
          </FormRow>
          <FormRow>
            <span>{t("addItem.shortDesc")}</span>
            <Input
              type="text"
              value={shortDescription}
              onChange={(e) => this.valueShortDescHandler(e)}
            />
          </FormRow>
          <FormRow>
            <span>{t("addItem.fullDesc")}</span>
            <Textarea
              name="text"
              cols="30"
              rows="5"
              value={fullDescription}
              onChange={(e) => this.valueFullDescHandler(e)}
            />
          </FormRow>
          <FormRow>
            <span>{t("addItem.category")}</span>
            <Select onChange={this.handleChange}>
              <option
                value="category1">
                Category1
              </option>
              <option
                value="category2">
                Category2
              </option>
            </Select>
          </FormRow>
          <UploadImages
            t={t}
            changeListImages={this.changeListImages}
            uploadedImageHandler={this.uploadedImageHandler}
            toggleErrorModal={toggleErrorModal}
            infoImages={infoImages}
            infoImagesArray={infoImagesArray}
            authUser={authUser}
            updateListImages={this.updateListImages}
            id={id}
          />
          <FormRow center>
            <Button
              type="submit"
              disabled={disable}>
              {t("addItem.submit")}
            </Button>
          </FormRow>
        </form>
      </Wrapper>
    )
  }
}

AddItem.propTypes = {
  authUser: PropTypes.object,
  loadListOfItems: PropTypes.func,
};

export default translate("common")(AddItem);