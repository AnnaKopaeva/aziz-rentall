import React, { Component } from "react";
import {db} from "../../firebase";
import { Wrapper, FormRow, Image } from "../../style/styles";

class DetailsInfo extends Component {
  state = {
    infoData: {},
  };

  updateInfoData = (infoData) => {
    this.setState({infoData})
  };

  componentDidMount(){
    const { authUser, id, toggleErrorModal } = this.props;
    const { updateInfoData } = this;

    const docRef = db.collection("items").doc(authUser.uid).collection("tasks").doc(id);

    docRef.get().then(doc => {
      if (doc.exists) {
        updateInfoData(doc.data());
      } else {
        toggleErrorModal("No such document!");
      }
    }).catch(error => {
      toggleErrorModal(error.message);
    });
  }

  render(){
    const { infoData } = this.state;
    let imagesList;
    if (infoData && infoData.infoImages && infoData.infoImages.length > 0) {
      imagesList = infoData.infoImages.map(image =>
        <Image key={image.key} src={image.url} />
      );
    }

    return(
      <Wrapper>
        <h3>Details:</h3>
        <FormRow>
          <span>Title</span>
          <span>{infoData.title}</span>
        </FormRow>
        <FormRow>
          <span>Category</span>
          <span>{infoData.category}</span>
        </FormRow>
        <FormRow>
          <span>Full Description</span>
          <span>{infoData.fullDescription}</span>
        </FormRow>
        <FormRow>
          <span>Short Description</span>
          <span>{infoData.shortDescription}</span>
        </FormRow>
        <FormRow>
          {imagesList}
        </FormRow>
      </Wrapper>
    )
  }
}

export default DetailsInfo;