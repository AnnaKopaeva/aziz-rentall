import React, { Fragment } from "react";
import { Link } from "react-router-dom";
import DetailsInfo from "./DetailsInfo";
import ErrorModal from "../../components/Modal/ErrorModal";

const Details = (props) => (
  <Fragment>
    <Link to="/">Home</Link>
    <DetailsInfo
      authUser={props.authUser}
      id={props.id}
      toggleErrorModal={props.toggleErrorModal}
    />
    <ErrorModal />
  </Fragment>
);

export default Details;