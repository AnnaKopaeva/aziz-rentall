import {db} from "../firebase";

const updateRealData = (authUser,listOfItems, firstVisible, numberPerPage, updateData, changeVisible, toggleErrorModal ) => {

  let query = db.collection("items").doc(authUser.uid).collection("tasks")
    .orderBy("id", "desc")
    .startAt(firstVisible)
    .limit(numberPerPage);

  if (listOfItems.length === 1) {
    query = db.collection("items").doc(authUser.uid).collection("tasks")
      .orderBy("id", "desc")
      .limit(numberPerPage);
  }

  query.get()
    .then(documentSnapshots => {
      const { docs } = documentSnapshots;
      const lastVisible = docs[docs.length - 1];
      const firstVisible = docs[0];

      if (docs.length !== 0) {
        let listOfItems = [];
        documentSnapshots.forEach(function (doc) {
          listOfItems.push({...doc.data(), autoId: doc.id});
        });
        updateData(listOfItems);
        changeVisible(lastVisible, firstVisible);
      }
    })
    .catch(() => toggleErrorModal("Error loading documents"))
};

export default updateRealData;