import {db} from "../firebase";

const applyFilter = (authUser, title, status, numberPerPage, changeStatusFilter) => {
  let query;
  let statusValue = status === "active" ? false : status === "suspended" ? true : null;

  if (title === "" && status !== "all") {
    changeStatusFilter(true);
    query= db.collection("items").doc(authUser.uid).collection("tasks")
      .where("suspended", "==", statusValue)

  } else if (status === "all") {
    changeStatusFilter(false);
    if (title === ""){
      query= db.collection("items").doc(authUser.uid).collection("tasks")
        .orderBy("id", "desc")
        .limit(numberPerPage);
    } else {
      changeStatusFilter(true);
      query = db.collection("items").doc(authUser.uid).collection("tasks")
        .where("title", "==", title)
    }
  } else {
    changeStatusFilter(true);
    query = db.collection("items").doc(authUser.uid).collection("tasks")
      .where("title", "==", title)
      .where("suspended", "==", statusValue)
  }

  return query.get()
    .then(documentSnapshots => {
      const { docs } = documentSnapshots;
      const lastVisible = docs[docs.length-1];
      const firstVisible = docs[0];

      let listOfItems = [];
      documentSnapshots.forEach(doc => {
        listOfItems.push({...doc.data(), autoId: doc.id})
      });
      return {
        listOfItems: listOfItems,
        lastVisible: lastVisible,
        firstVisible: firstVisible
      }
    })
};

export default applyFilter;
