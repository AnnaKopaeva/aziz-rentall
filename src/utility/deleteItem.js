import {db} from "../firebase";
import firebase from 'firebase';

const deleteItem = (authUser, listOfItems, autoId, id, updateData, updateRealData, toggleErrorModal ) => {
  const updateListOfItems = listOfItems.filter((obj) => {
    return obj.id !== id;
  });

  const withoutDeleteItems = listOfItems.filter((obj) => {
    return obj.id === id;
  });

  db.collection("items").doc(authUser.uid).collection("tasks").doc(autoId).delete()
    .then(() => {
      updateData(updateListOfItems);
      updateRealData();

      if (withoutDeleteItems[0].infoImages.length > 0) {
        withoutDeleteItems[0].infoImages.map(image => {
          firebase.storage().refFromURL(image.url).delete().then(() => {
            console.log("File deleted successfully")
          }).catch((error) => {
            toggleErrorModal(error.message);
          });
        });
      }
    })
    .catch(error => {
      toggleErrorModal(error.message)
    });
};

export default deleteItem;