import {db} from "../firebase";

const uploadPrevData = (authUser, firstVisible, numberPerPage) => {
  const prev = db.collection("items").doc(authUser.uid).collection("tasks")
    .orderBy("id")
    .startAfter(firstVisible)
    .limit(numberPerPage);

  return prev.get()
    .then(documentSnapshots => {
      const { docs } = documentSnapshots;
      const firstVisible = docs[docs.length-1];
      const lastVisible = docs[0];

      if (docs.length !== 0) {
        let arrItems = [];
        documentSnapshots.forEach(doc => {
          arrItems.push({...doc.data(), autoId: doc.id});
        });

        return {
          arrItems: arrItems,
          lastVisible: lastVisible,
          firstVisible: firstVisible
        }
      }
    })
};

export default uploadPrevData;