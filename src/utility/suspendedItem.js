import {db} from "../firebase";

const suspendedItem = (authUser, listOfItems, autoId, id, suspended, updateData, toggleErrorModal ) => {
  const updateListOfItems = listOfItems.find((obj) => {
    return obj.id === id;
  });
  updateListOfItems.suspended = !suspended;

  db.collection("items").doc(authUser.uid).collection('tasks').doc(autoId)
    .onSnapshot(() => {
      updateData(listOfItems);
    });

  db.collection("items").doc(authUser.uid).collection("tasks").doc(autoId).update({
    suspended: !suspended
  })
    .catch((error) => {
      toggleErrorModal(error.message)
    });
};

export default suspendedItem;
