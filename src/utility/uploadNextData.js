import {db} from "../firebase";

const uploadNextData = (authUser, lastVisible, numberPerPage) => {
  const next = db.collection("items").doc(authUser.uid).collection("tasks")
    .orderBy("id", "desc")
    .startAfter(lastVisible)
    .limit(numberPerPage);

  return next.get()
    .then(function (documentSnapshots) {
      const lastVisible = documentSnapshots.docs[documentSnapshots.docs.length - 1];
      const firstVisible = documentSnapshots.docs[0];

      if (documentSnapshots.docs.length !== 0) {
        let arrItems = [];
        documentSnapshots.forEach(doc => {
          arrItems.push({...doc.data(), autoId: doc.id});
        });
        return {
          arrItems: arrItems,
          lastVisible: lastVisible,
          firstVisible: firstVisible
        }
      }
    })
};

export default uploadNextData;