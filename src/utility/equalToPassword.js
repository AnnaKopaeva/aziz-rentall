import { mixed } from "yup";

const equalTo = (ref, msg) => {
  return mixed().test({
    name: "equalTo",
    exclusive: false,
    message: msg || `${msg} must be the same as ${ref}`,
    params: {
      reference: ref.path,
    },
    test: function(value) {
      return value === this.resolve(ref);
    },
  });
};

export default equalTo;