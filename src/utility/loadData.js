import {db} from "../firebase";

const loadData = (authUser, numberPerPage) => {
  const query = db.collection("items").doc(authUser.uid).collection("tasks")
    .orderBy("id", "desc")
    .limit(numberPerPage);

  return query.get()
    .then(documentSnapshots => {
      const { docs } = documentSnapshots;
      const lastVisible = docs[docs.length-1];
      const firstVisible = docs[0];

      let listOfItems = [];
      documentSnapshots.forEach(doc => {
        listOfItems.push({...doc.data(), autoId: doc.id})
      });
      return {
        listOfItems: listOfItems,
        lastVisible: lastVisible,
        firstVisible: firstVisible
      }
    })
};

export default loadData;
