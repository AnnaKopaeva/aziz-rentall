import React from 'react';

const FilterContext = React.createContext({
  title: "",
  status: "all",
  changeTitle: () => {},
  changeStatus: () => {},
  filter: false,
  changeStatusFilter: () => {},
});

export default FilterContext;