import React from 'react';

const modalContext = React.createContext({
  errorModal: false,
  errorDescription: "",
  toggleErrorModal: () => {}
});

export default modalContext;