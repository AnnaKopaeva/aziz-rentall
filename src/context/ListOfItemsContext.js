import React from "react";

const ListOfItemsContext = React.createContext({
  listOfItems: [],
  updateListOfItems: () => {},
  changeVisible: () => {},
});

export default ListOfItemsContext;
