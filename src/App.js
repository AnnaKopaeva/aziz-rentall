import React from 'react';
import { BrowserRouter as Router, Route,} from 'react-router-dom';
import './style/styles.sass';
import './style/reset.css';

import Main from "./containers/Main/MainLogic";
import Details from "./containers/Details/DetailsLogic";
import withAuthentication from './withAuthentication';
import withError from './withError';
import AddItem from "./containers/AddItems/AddItemsLogic";
import Login from "./containers/Auth/Login/LoginLogic";
import SignUp from "./containers/Auth/SignUp/SignUpLogic";
import withApplyFilter from "./withApplyFilter";

const App = () => (
  <Router>
    <div>
      <Route
        path="/login"
        component={Login}
      />
      <Route
        path="/signup"
        component={SignUp}
      />
      <Route
        exact path="/"
        component={Main}
      />
      <Route
        path="/view/:id"
        component={Details}
      />
      <Route
        path="/add"
        component={AddItem}
      />
    </div>
  </Router>
);

export default withAuthentication(withError(withApplyFilter(App)));
