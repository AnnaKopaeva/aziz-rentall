import React from 'react';
import FilterContext from './context/FilterContext';

const withApplyFilter = (Component) => (
  class withApplyFilter extends React.Component {
    changeTitle = (event) => {
      this.setState({title: event.target.value})
    };

    changeStatus = (event) => {
      this.setState({status: event.target.value})
    };

    changeStatusFilter = (filter) => {
      this.setState({filter})
    };

    state = {
      title: "",
      status: "all",
      changeTitle: this.changeTitle,
      changeStatus: this.changeStatus,
      filter: false,
      changeStatusFilter: this.changeStatusFilter
    };

    render() {
      return (
        <FilterContext.Provider value={this.state}>
          <Component />
        </FilterContext.Provider>
      );
    }
  }
);

export default withApplyFilter;