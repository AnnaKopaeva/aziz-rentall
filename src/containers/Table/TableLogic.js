import React, { Component } from 'react';
import PropTypes from "prop-types";
import ListOfItemsContext from "../../context/ListOfItemsContext";
import FilterContext from "../../context/FilterContext";
import ModalContext from "../../context/ModalContext";
import Table from "../../components/Table/Table";

class TableLogic extends Component {
  render() {
    const { authUser } = this.props;
    return (
      <FilterContext.Consumer>
        {valueFilter =>
          <ListOfItemsContext.Consumer>
            {value =>
              <ModalContext.Consumer>
                {valueModal =>
                  <Table
                    authUser={authUser}
                    listOfItems={value.listOfItems}
                    numberPerPage={value.numberPerPage}
                    updateData={value.updateListOfItems}
                    changeVisible={value.changeVisible}
                    toggleErrorModal={valueModal.toggleErrorModal}
                    firstVisible={value.firstVisible}
                    lastVisible={value.lastVisible}
                    filter={valueFilter.filter}
                  />
                }
              </ModalContext.Consumer>
            }
          </ListOfItemsContext.Consumer>
        }
      </FilterContext.Consumer>
    )
  }
}

Table.propTypes = {
  numberPerPage: PropTypes.number,
  authUser: PropTypes.object,
  updateData: PropTypes.func,
};

export default TableLogic;