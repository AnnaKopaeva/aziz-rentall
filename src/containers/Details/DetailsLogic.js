import React, {Fragment} from "react";
import Details from "../../components/Details/Details";
import AuthUserContext from "../../context/AuthUserContext";
import ModalContext from "../../context/ModalContext";
import Header from "../../components/Header/Header";

const DetailsLogic = (props) => (
  <ModalContext.Consumer>
    {value =>
      <AuthUserContext.Consumer>
        {authUser =>
          <Fragment>
            <Header authUser={authUser} />
            { authUser ?
             <Details
               toggleErrorModal={value.toggleErrorModal}
               authUser={authUser}
               id={props.match.params.id}
             />
            : null}
          </Fragment>
        }
      </AuthUserContext.Consumer>
    }
  </ModalContext.Consumer>
);

export default DetailsLogic;