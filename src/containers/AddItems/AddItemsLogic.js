import React, {Component, Fragment} from 'react';
import PropTypes from "prop-types";
import { translate } from "react-i18next";
import AddItem from "../../components/AddItem/AddItem"
import AuthUserContext from "../../context/AuthUserContext"
import ModalContext from "../../context/ModalContext"
import ErrorModal from "../../components/Modal/ErrorModal";
import Header from "../../components/Header/Header";

class AddItemLogic extends Component {
  render(){
    const { t, history } = this.props;

    return (
      <AuthUserContext.Consumer>
        { authUser =>
          <ModalContext.Consumer>
            {value =>
              <Fragment>
                <Header authUser={authUser} />
                {authUser ?
                  <AddItem
                    history={history}
                    authUser={authUser}
                    toggleErrorModal={value.toggleErrorModal}
                    t={t}
                  /> : null }
                  <ErrorModal />
              </Fragment>
            }
          </ModalContext.Consumer>
        }
      </AuthUserContext.Consumer>
    )
  }
}

AddItemLogic.propTypes = {
  authUser: PropTypes.object,
  loadListOfItems: PropTypes.func,
};

export default translate("common")(AddItemLogic);