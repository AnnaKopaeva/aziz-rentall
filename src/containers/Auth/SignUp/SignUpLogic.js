import React from "react";
import ModalContext from "../../../context/ModalContext";
import SignUp from "../../../components/Auth/SignUp/SignUp";
import PropTypes from "prop-types";

const SignUpLogic = () => (
  <ModalContext.Consumer>
    { value => (
      <SignUp
        toggleErrorModal={value.toggleErrorModal}
      />
    )}
  </ModalContext.Consumer>
);

SignUp.propTypes = {
  toggleModal: PropTypes.func,
};

export default SignUpLogic;
