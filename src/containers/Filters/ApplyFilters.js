import React, { Component } from 'react';
import FilterData from "../../components/Table/FilterData";
import FilterContext from "../../context/FilterContext";
import applyFilter from "../../utility/applyFilter";

class ApplyFilters extends Component {
  applyFilters = () => {
    const { title, status } = this.props;
    const { authUser, numberPerPage, toggleErrorModal, updateData, changeVisible, changeStatusFilter } = this.props;

    applyFilter(authUser, title, status, numberPerPage, changeStatusFilter)
      .then(list => {
        updateData(list.listOfItems);
        changeVisible(list.lastVisible, list.firstVisible);
      })
      .catch(() => toggleErrorModal("Error loading documents"));
  };

  render(){
    const { authUser, numberPerPage, toggleErrorModal, updateData, changeVisible } = this.props;
    return (
      <FilterContext.Consumer>
        {value => (
          <FilterData
            title={value.title}
            status={value.status}
            changeTitle={value.changeTitle}
            changeStatus={value.changeStatus}
            applyFilters={this.applyFilters}
            authUser={authUser}
            numberPerPage={numberPerPage}
            toggleErrorModal={toggleErrorModal}
            updateData={updateData}
            changeVisible={changeVisible}
          />
          )
        }
      </FilterContext.Consumer>
    )
  }
}

export default ApplyFilters;