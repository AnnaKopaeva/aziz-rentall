import React, { Component } from 'react';
import AuthUserContext from "../../context/AuthUserContext";
import ListOfItemsContext from "../../context/ListOfItemsContext";
import loadData from "../../utility/loadData";
import Content from "../../components/Main/Content";

class ContentLogic extends Component {
  updateListOfItems = (listOfItems) => {
    this.setState({listOfItems})
  };

  changeVisible = (lastVisible, firstVisible) => {
    this.setState({lastVisible, firstVisible})
  };

  state = {
    listOfItems: [],
    numberPerPage: 10,
    lastVisible: null,
    firstVisible: null,
    updateListOfItems: this.updateListOfItems,
    changeVisible: this.changeVisible,
    loading: false
  };

  componentDidMount(){
    this.loadListOfItems()
  }

  loadListOfItems = () => {
    const { numberPerPage } = this.state;
    const { authUser, toggleErrorModal } = this.props;
    const { changeVisible, updateListOfItems } = this;

    const loadingList = (loading) => {
      this.setState({loading});
    };

    loadingList(true);
    loadData(authUser, numberPerPage)
      .then((list) => {
        updateListOfItems(list.listOfItems);
        changeVisible(list.lastVisible, list.firstVisible);
        loadingList(false);
      })
      .catch(() => toggleErrorModal("Error loading documents"));
  };

  render() {
    const { loading } = this.state;
    return (
      <ListOfItemsContext.Provider value={this.state}>
        <AuthUserContext.Consumer>
          { authUser =>
           <Content
             authUser={authUser}
             loading={loading}
           />
          }
        </AuthUserContext.Consumer>
      </ListOfItemsContext.Provider>
    )
  }
}

export default ContentLogic;