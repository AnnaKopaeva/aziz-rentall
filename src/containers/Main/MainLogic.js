import React, { Component } from 'react';
import ModalContext from "../../context/ModalContext";
import AuthUserContext from "../../context/AuthUserContext";
import Main from "../../components/Main/Main";

class MainLogic extends Component {
  render(){
    return(
      <ModalContext.Consumer>
        {value =>
          <AuthUserContext.Consumer>
            {authUser =>
              <Main
                authUser={authUser}
                toggleModal={value.toggleModal}
                toggleErrorModal={value.toggleErrorModal}
              />
            }
          </AuthUserContext.Consumer>
        }
      </ModalContext.Consumer>
    )
  }
}

export default MainLogic;