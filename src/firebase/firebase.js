import firebase from 'firebase'
import 'firebase/auth';

const config = {
  apiKey: "AIzaSyA2K8vth8AXhZgdvJf5z8oX6j6RuYstfOs",
  authDomain: "rentall-dd6c7.firebaseapp.com",
  databaseURL: "https://rentall-dd6c7.firebaseio.com",
  projectId: "rentall-dd6c7",
  storageBucket: "rentall-dd6c7.appspot.com",
  messagingSenderId: "953759884506"
};
firebase.initializeApp(config);

export const auth = firebase.auth();
const db = firebase.firestore();
db.settings({timestampsInSnapshots: true});
export {firebase, db}
